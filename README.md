
# :frog: RECAST :frog:

More info in the whole RECAST project can be found
https://recast-docs.web.cern.ch/recast-docs/#tldr

This example is a simple way on how to run a RECAST workflow for your event selection + statistical analysis (HistFitter) code.
The example shown here is the ones used for the higgsinos with soft leptons HL-LHC prospects ATL-PHYS-PUB-2018-031 or arXiv:1812.07831.

The events selection analysis repository can be found in:
<https://gitlab.cern.ch/jsabater/higgsino-upgrade.git>

The statistical analysis repository can be found in:
<https://gitlab.cern.ch/jsabater/higgsino-upgrade-hf.git>

You can run both independently (e.g. to adapt your analysis) following the instructions given in the repositories.

## Quick description of the files:

*  specs/steps.yml : this is where you specify the steps of the analysis, i.e., the command lines you normally run in the terminal.
*  specs/workflow.yml : this should be your workflow. It connects the output from the event selection with the input of the statistical analysis.
*  recast.yml : here you specify the parameters that are going to be used by steps.yml.

## Preparation
:warning: WARNING: this is intended to be run locally in your laptop (not e.g. in lxplus) :warning:

(Read [preparation.txt](./preparation.txt) for details)
*  Install docker from https://docs.docker.com/install/
*  Install pip
*  Install virtualenv and recast-atlas
```
pip install virtualenv
virtualenv venv
source venv/bin/activate
pip install recast-atlas
```
*  Create a personal access token on github : https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html


### At the beginning of each session you should
```
virtualenv venv
source venv/bin/activate
pip install recast-atlas==0.0.20
```


### To run

:warning: This example uses files under `/eos/user/s/susyupg/` , to access these contact <jorge.sabater.iglesias@desy.de> :warning:

```
git clone https://gitlab.cern.ch/jsabater/higgsino-upgrade.git
# If you are going to use files in eos space (which is the case in the example), you will need to setup the following
RECAST_USER=myUser # eos user (e.g. susyupg)
RECAST_PASS=myPass # password
RECAST_TOKEN=myToken # git token
eval "$(recast auth setup -a $RECAST_USER -a $RECAST_PASS -a $RECAST_TOKEN -a default)"
eval "$(recast auth write --basedir authdir)"


# add the project
$(recast catalogue add higgsino-upgrade-workflow)
# check that it was added
recast catalogue ls
# check that all is ok
recast catalogue check examples/higgsinos
# check the steps were added
recast tests ls examples/higgsinos

# to run the workflow non-interactively (10k events default)
recast run examples/higgsinos --example default
# you can also run over lower number of events for testing
recast run examples/higgsinos --example alternative
```
After this you should have a new directory called `recast-*`.
The results of the run will be in `recast-*/`. There you will find the output of the event selection (`higgsinos/`) and HF steps (`higgsinos-hf/`).
If something went wrong during the run, or you want to check the output, you can take a look at the log files in `recast-*/higgsinos/_packtivity` or `recast-*/higgsinos-hf/_packtivity`.

To run the event selection and the statistical analysis independently,
```
# check the steps
recast tests ls examples/higgsinos
# to run the event selection
recast tests run examples/higgsinos test_eventselection
# to run the statistical analysis
recast tests run examples/higgsinos test_statanalysis
```


## Running in git CI
To run the analysis in git (using git ci) you will have to fill the fields in 

Settings -> CI/CD -> Variables 

KRB_PASS # pwd 

RECAST_TOKEN # token 

KRB_USER # user

I changed the variable names RECAST_PASS (KRB_PASS) and RECAST_USER (KRB_USER) from above for testing reasons. Just make sure the variable names are consistent with the ones in the [gitlab-ci](./.gitlab-ci.yml) file.

